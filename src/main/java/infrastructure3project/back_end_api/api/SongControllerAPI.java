package infrastructure3project.back_end_api.api;


import infrastructure3project.back_end_api.model.Song;
import infrastructure3project.back_end_api.repository.SongDTO;
import infrastructure3project.back_end_api.service.SongService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/song")
public class SongControllerAPI {


    private final SongService songService;

    public SongControllerAPI(SongService songService) {
        this.songService = songService;
    }



    // Search Artists
    @GetMapping
    public ResponseEntity<List<Song>> searchSongs(@RequestParam("searchTerm") final String searchTerm){
        List<Song> songList = songService.searchSongs(searchTerm);


        List<SongDTO> songDTOList = songList
                .stream()
                .map(song -> new SongDTO(song.getId(), song.getName(), song.getDuration()))
                .toList();

        return ResponseEntity.ok(songList);
    }


}
