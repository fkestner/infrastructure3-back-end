package infrastructure3project.back_end_api.repository;


import infrastructure3project.back_end_api.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SongRepository extends JpaRepository<Song, Integer> {



    List<Song> findAllByNameContainingIgnoreCase(String name);
}
