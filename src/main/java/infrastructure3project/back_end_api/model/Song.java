package infrastructure3project.back_end_api.model;


import jakarta.persistence.*;

@Entity
public class Song {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(name = "bucket_link", nullable = false)
    private String bucketLink;

    @Column(nullable = false)
    private int duration;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBucketLink() {
        return bucketLink;
    }

    public void setBucketLink(String bucketLink) {
        this.bucketLink = bucketLink;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}