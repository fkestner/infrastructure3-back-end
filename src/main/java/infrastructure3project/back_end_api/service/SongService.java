package infrastructure3project.back_end_api.service;


import infrastructure3project.back_end_api.model.Song;
import infrastructure3project.back_end_api.repository.SongRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SongService {



    private final SongRepository songRepository;


    public SongService(SongRepository songRepository) {
        this.songRepository = songRepository;
    }

    public List<Song> searchSongs(String searchTerm) {
        List<Song> songList = songRepository.findAllByNameContainingIgnoreCase(searchTerm);
        return songList;
    }
}
