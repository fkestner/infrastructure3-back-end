package infrastructure3project.back_end_api.repository;

public class SongDTO {

    public int id;

    public String name;

    public int duration;


    public SongDTO(int id, String name, int duration) {
        this.id = id;
        this.name = name;
        this.duration = duration;
    }
}
